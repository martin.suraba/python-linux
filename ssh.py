#!/usr/bin/python3


import paramiko


# type of servers

servers = [ "8.8.8.8", "7.7.7.7" ]



# where is the key?

key_file = "/root/.ssh/id_rsa"


#go to servers

for server in servers:
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(server, username="root", key_filename=key_file)


# command on the server

    stdin, stdout, stderr = client.exec_command("uptime")
#    stdin, stdout, stderr = client.exec_command("hostname")


# the output of command

    print(f"{server}")
#    print(server)
    print(stdout.read().decode())

# exit from server
    client.close()
